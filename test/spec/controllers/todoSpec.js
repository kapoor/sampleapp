'use strict';

describe('Controller: ToDoCtrl', function () {

  // load the controller's module
  beforeEach(module('sampleApp'));

  var ToDoCtrl,
    scope;

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();

    ToDoCtrl = $controller('ToDoCtrl', {
      $scope: scope
    });



  }));

  it('should define initial declaration (todoList, newTodo etc etc) to the scope', function () {
    expect(scope.todoList.length).toEqual(0);
    expect(scope.newTodo.item).toEqual('');

  });
  it('create function should add an item to the to "todoList"',function(){
    scope.newTodo.item = "test2"
    scope.create();
    expect(scope.todoList.length).toEqual(1);
  });
  it('delete function should delete an item from the  "todoList"',function(){
    scope.newTodo.item = "some text";
    scope.create();
    scope.delete("some text");
    expect(scope.todoList.length).toEqual(0);
  });
});
