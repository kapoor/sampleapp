'use strict';

describe('Controller: flotCtrl', function () {

  // load the controller's module
  beforeEach(module('sampleApp'));

  var flotCtrl,
    scope, backendservice;
  var data = [
    {
      "year": 2000,
      "average_temp": 23
    },
    {
      "year": 2001,
      "average_temp": 17
    },
    {
      "year": 2002,
      "average_temp": 12
    },
    {
      "year": 2003,
      "average_temp": 34
    },
    {
      "year": 2004,
      "average_temp": 23
    },
    {
      "year": 2005,
      "average_temp": 30
    },
    {
      "year": 2006,
      "average_temp": 27
    },
    {
      "year": 2007,
      "average_temp": 15
    },
    {
      "year": 2008,
      "average_temp": 34
    },
    {
      "year": 2009,
      "average_temp": 41
    },
    {
      "year": 2010,
      "average_temp": 32
    }
  ];

  // Initialize the controller and a mock scope
  beforeEach(inject(function ($controller, $rootScope) {
    scope = $rootScope.$new();
    backendservice = {
      getChartData: function () {
        return data;
      }
    };
    flotCtrl = $controller('flotCtrl', {
      $scope: scope
    });
    spyOn(backendservice, 'getChartData').callThrough();
  }));

  it('checks backend service to have been called with', function () {
    expect(backendservice.getChartData()).toHaveBeenCalled();
  });
});
