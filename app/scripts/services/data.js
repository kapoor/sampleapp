'use strict';

/*
 * @angular service to talk to backend rest-api if avaiable
 * Temporarily fetching data from static json file.
 * */

var app = angular.module('sampleApp');

app.factory('BackendService', function ($http, $q) {
  return ({
    getChartData: getChartData
  });

  function getChartData() {
    return getHttp('data/chartData.json');
  }

  function getHttp(url) {

    var request = $http({
      method: 'GET',
      url: url
    });

    return (request.then(success, error));

    function success(response) {
      return (response.data);
    }

    function error(response) {
      return ($q.reject(response.data.message));
    };
  }

})
