'use strict';

/**
 * @ngdoc overview
 * @name sampleappApp
 * @description
 * # sampleappApp
 *
 * Main module of the application.
 */

angular
  .module('sampleApp', [
    'ngResource',
    'ngRoute',
    'xeditable',
    'angular-flot',
    'LocalStorageModule'
  ])
  .config(['localStorageServiceProvider',function(localStorageServiceProvider){
    localStorageServiceProvider.setPrefix('kap');
  }])
  .config(function ($routeProvider) {
    $routeProvider
      .when('/', {
        templateUrl: 'views/todo.html',
        controller: 'ToDoCtrl'
      })
      .when('/flot', {
        templateUrl: '/views/flotChart.html',
        controller: 'flotCtrl'
      })
      .otherwise({
        redirectTo: '/'
      });
  });

angular.module('sampleApp').run(function(editableOptions) {
  editableOptions.theme = 'bs3'; // bootstrap3 theme. Can be also 'bs2', 'default'
});
