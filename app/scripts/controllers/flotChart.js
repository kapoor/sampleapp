'use strict';

/**
 * @ngdoc function
 * @name sampleApp.controller:flotCtrl
 * @description
 * # flotCtrl
 * Controller of the sampleApp
 */
var app = angular.module('sampleApp');
app.controller('flotCtrl', ['$scope', 'BackendService', function ($scope, BackendService) {
  var dataSet = [];
  var options =  {
    legend: {
      container: "#legend",
      show: true
    }
  }
  BackendService.getChartData().then(function (data) {
    $scope.data = data;
    angular.forEach(data, function (value, key) {
      dataSet.push([value.year, value.average_temp]);
    });
    $scope.dataSet = [{data:dataSet}];
    $scope.options = options;
  });

}]);
