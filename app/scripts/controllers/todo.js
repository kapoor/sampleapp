'use strict';

/**
 * @ngdoc function
 * @name sampleApp.controller:ToDoCtrl
 * @description
 * # ToDoCtrl
 * Controller of the sampleApp
 */
 var app = angular.module('sampleApp');
 app.controller('ToDoCtrl', function ($scope,localStorageService) {
   var todoListInLocalStorage = localStorageService.get('todoList');
 	$scope.todoList = todoListInLocalStorage || [];
 	$scope.newTodo = {};
 	$scope.newTodo.item = '';


 	$scope.create = function(){
 		if($scope.newTodo.item !== '')
 			$scope.todoList.push($scope.newTodo.item);

 		$scope.newTodo.item ='';
 	};

 	$scope.delete = function(item){
 		var index = $scope.todoList.indexOf(item);
 		$scope.todoList.splice(index,1);
 	};

   $scope.$watch('todoList', function () {
     localStorageService.set('todoList', $scope.todoList);
   }, true);


 });

